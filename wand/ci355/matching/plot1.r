## 
## Jonathan Wand : 2006-02-26
## Graphic example of distance calculations
##  potentially used for matching
## 

library(ellipse)

cormat <- matrix( c(1,.9,.9,1),ncol=2)

plot(ellipse( cormat,scale=c(3,3) ),type="l",frame.plot=FALSE,
     xlab="X1",ylab="X2")
points( 0, 0 , col="green",lwd=3,cex=1.5)
points( 5, 5 , col="blue",lwd=3,cex=1.5)
points( 4, 0 , col="red",lwd=3,cex=1.5)
abline( v=0,col="grey")
abline( h=0,col="grey")


lines( c(0,5), c(0,5), lty=2, lwd = 3)
lines( c(0,4), c(0,0), lty=3, lwd = 3)

## let's calculate the distance metrics...
sqrt( t(c(5,5)) %*% solve(cormat) %*% c(5,5) )
sqrt( t(c(4,0)) %*% solve(cormat) %*% c(4,0) )

sqrt( t(c(5,5)) %*% solve(diag(diag(cormat))) %*% c(5,5) )
sqrt( t(c(4,0)) %*% solve(diag(diag(cormat))) %*% c(4,0) )

## a tranformation

xj <- c(5,5)
xk <- c(0,4)

thefunc <- function(x,rho)  (x[1] - x[2]*rho) / ( sqrt( 1-rho^2) )

xj2 <- c(xj[1] ,  thefunc(xj,.9) )
xk2 <- c(xk[1] ,  thefunc(xk,.9) )

sqrt( t(xj2) %*% solve(cormat) %*% xj2 )
sqrt( t(xk2) %*% solve(cormat) %*% xk2 )

sqrt( t(xj2) %*% solve(diag(diag(cormat))) %*% xj2 )
sqrt( t(xk2) %*% solve(diag(diag(cormat))) %*% xk2 )

