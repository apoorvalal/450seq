---
title: Stanford Methodology / Econometrics Sequence Slides
---

# 450a : Regression

+ [intro](450a/lec1_nopause.pdf)
+ [point estimation](450a/lec21_nopause.pdf)
+ [interval estimation](450a/lec22_nopause.pdf)
+ [hypothesis testing](450a/lec23_nopause.pdf)
+ [uncertainty](450a/lec24_nopause.pdf)
+ [regression (summation form)](450a/lec3_nopause.pdf)
  + [univariate](450a/lec4_nopause.pdf)
  + [bivariate](450a/lec51_nopause.pdf)
  + [OVB](450a/lec52_nopause.pdf)
  + [dummies, interactions, polynomials](450a/lec53_nopause.pdf)
+ [regression (matrix form)](450a/lec61_nopause.pdf)
+ [regression inference](450a/lec62_nopause.pdf)
+ diagnostics
  + [nonnormality](450a/lec71_nopause.pdf)
  + [misspecification](450a/lec72_nopause.pdf)
  + [heteroskedasticity / autocorrelation](450a/lec73_nopause.pdf)
  + [measurement error](450a/lec74_nopause.pdf)
+ [binary DVs](450a/lec8_nopause.pdf)


# 450b : Causal Inference

## Cross-sectional
+ [Introduction](450b/intro_2021_nopause.pdf)
+ [Potential Outcomes](450b/po_model_2021_nopause.pdf)
+ [Experiments](450b/re_nopause.pdf)
  + [Experiments P1](450b/re_partA1_nopause.pdf)
  + [Experiments P2](450b/re_partB1_nopause.pdf)
+ [Selection on Observables](450b/selection_on_observables1_nopause.pdf)
  + [Sensitivity analysis for SOO](450b/SOOSensitivity_nopause.pdf)
+ [IV basics](450b/IV_1_nopause.pdf)
+ [IV - LATE](450b/IV_2_nopause.pdf)
+ [Regression Discontinuity](450b/RDD1_nopause.pdf)
+ [Quantile Regression](450b/QREG1_nopause.pdf)
  + [V2](450b/QREG2018_nopause.pdf)


## Panel

+ [Difference in Differences](450b/DID_nopause.pdf)
+ [Panel Data](450b/Panel1_nopause.pdf)
  * [Synthetic Control](450b/SCM_nopause.pdf)
  * [Yiqing - SCM](450b/YXU_panel_nopause.pdf)

+ [Summary](450b/SUMMARY_1_nopause.pdf)

# 450c : Machine Learning

## Basics / CEF fitting

+ [Introduction](450c/intro_nopause.pdf)
+ [Maximum Likelihood](450c/likelihood1_nopause.pdf)
+ [Probit/Logit](450c/probit_nopause.pdf)
+ [Goodness of fit for binary prediction](450c/BinaryFit_nopause.pdf)
+ [Bootstrap](450c/Bootstrap_nopause.pdf)
+ [PCA](450c/pca_nopause.pdf)
+ [Ridge/LASSO](450c/ridge_nopause.pdf)
+ [Ensembles and Random Forests](450c/ensemble_tree_nopause.pdf)
+ [GAMs](450c/gam_final_nopause.pdf)




## Unsupervised Learning

+ [Text Processing](450c/process_nopause.pdf)
+ [Naive Bayes](450c/naive_nopause.pdf)
+ [Clustering](450c/cluster_nopause.pdf)
+ [LDA](450c/lda_nopause.pdf)
+ [Fictitious Prediction](450c/fict_nopause.pdf)

## Causal Inference

+ [Double ML](450c/double_nopause.pdf)
+ [Causal Inference with text](450c/CausalText_21_nopause.pdf)
+ [Heterogeneous Effects](450c/het_21_nopause.pdf)
+ [Deconfounder](450c/blessings_naive_nopause.pdf)

### 2019 version

+ [intro](450c/2019/1intro.pdf)
+ [maximum likelihood and bernoulli](450c/2019/2_MaxLikGeneral_Bern.pdf)
+ [OLS in likelihood form + newton raphson](450c/2019/3_OLSMaxLik_NewtonRaph.pdf)
+ [Logit_Probit](450c/2019/4_Logit_Probit.pdf)
+ [Variance estimation : bootstrap / delta method](450c/2019/5_deltaBootstrapMNLSim.pdf)
+ [Ordered Logits, perfect separation](450c/2019/6_Separation_OLog.pdf)
+ [Multinomial logit ](450c/2019/7_MNL.pdf)
+ [Multinomial Probit + Counts](450c/2019/8_MNP_Poisson.pdf)
+ [clustering and survival I](450c/2019/9_cluster_survival.pdf)
+ [survival II + 3 tests](450c/2019/10_survival_hypothesis.pdf)
+ [3tests + Linear Exponential Family](450c/2019/11hyp_model.pdf)
+ [gam](450c/2019/12gam_final.pdf)
+ [PCA](450c/2019/13PCA.pdf)
+ [RidgeLasso](450c/2019/14RidgeLasso.pdf)
+ [BinaryGoodnessFit](450c/2019/15BinaryGoodnessFit.pdf)
+ [ChoosingLambda](450c/2019/16ChoosingLambda.pdf)
+ [SVM_Kernel](450c/2019/17SVM_Kernel.pdf)

# 450d : Bayesian Statistics

+ [intro](450d/lec01.pdf)
+ [binomial](450d/lec02.pdf)
+ [normal](450d/lec03.pdf)
+ [priors for variances](450d/lec04.pdf)
+ [multi-parameter models](450d/lec05.pdf)
+ [intro hierarchical models](450d/lec06.pdf)
+ [hierarchical models](450d/lec07.pdf)
+ [mcmc](450d/lec09.pdf)
+ [gibbs and metropolis](450d/lec10.pdf)
+ [stan_tutorial](450d/stan_tutorial.pdf)
+ [bayesian regression](450d/lec11.pdf)
+ [model checking](450d/lec12.pdf)
+ [hierarchical linear models](450d/lec13.pdf)
+ [hierarchical linear models II](450d/lec14.pdf)

## JH version

+ [Reweighting](450d/JH_1.pdf)
+ [DAGs](450d/JH_2.pdf)
+ [Mediation](450d/JH_3.pdf)
+ [Conjoints](450d/JH_4.pdf)

# Misc

## Wand : Nonparametrics and Shape constrained estimation

+ [basics](wand/nonparametricsshapes/lecture01print.pdf)
+ [binary choice models](wand/nonparametricsshapes/lecture02print.pdf)
+ [multinomial choice I](wand/nonparametricsshapes/lecture04print.pdf)
+ [multinomial choice II](wand/nonparametricsshapes/lecture05print.pdf)
+ [multinomial choice (endogeneity) III](wand/nonparametricsshapes/lecture06print.pdf)
+ [choice with endogeneity - heckit](wand/nonparametricsshapes/lecture07print.pdf)
+ [LATE](wand/nonparametricsshapes/lecture08print.pdf)
+ [ordered means](wand/nonparametricsshapes/lecture09print.pdf)
+ [testing shape constraints, splines](wand/nonparametricsshapes/lecture10.pdf)
+ [quadratic programming + constraints](wand/nonparametricsshapes/lecture11print.pdf)
+ [b-splines](wand/nonparametricsshapes/lecture12print.pdf)
+ [KLIC](wand/nonparametricsshapes/lecture13print.pdf)
+ [Model selection](wand/nonparametricsshapes/lecture14print.pdf)
+ [RD](wand/nonparametricsshapes/lecture15print.pdf)

## Wand - 350c

+ [Potential Outcomes](wand/350c/causal.pdf)
+ [Discrete Choice](wand/350c/lecture1.pdf)
+ [Discrete Choice II](wand/350c/lecture2.pdf)
+ [MLE](wand/350c/lecture3.pdf)
+ [Durations and Counts](wand/350c/time.pdf)

# Imbens

## 272 (1st year)

+ [1_experiments](imbens/272/1_experiments.pdf)
+ [2_power_strat](imbens/272/2_power_strat.pdf)
+ [3_cluster_random](imbens/272/3_cluster_random.pdf)
+ [4_normal_linear](imbens/272/4_normal_linear.pdf)
+ [5_regression_adjustment](imbens/272/5_regression_adjustment.pdf)
+ [6_bootstrap](imbens/272/6_bootstrap.pdf)
+ [7_Linear_Clustered](imbens/272/7_Linear_Clustered.pdf)
+ [8_regularised_regs](imbens/272/8_regularised_regs.pdf)
+ [9_quantile_reg](imbens/272/9_quantile_reg.pdf)
+ [10_bayes](imbens/272/10_bayes.pdf)
+ [11_bayes_hierarchical](imbens/272/11_bayes_hierarchical.pdf)
+ [12_GMM](imbens/272/12_GMM.pdf)
+ [13_GMM_EL](imbens/272/13_GMM_EL.pdf)
+ [14_Unconf](imbens/272/14_Unconf.pdf)
+ [15_Unconf_II](imbens/272/15_Unconf_II.pdf)
+ [16_DiD](imbens/272/16_DiD.pdf)
+ [16_DiD_Synth2](imbens/272/16_DiD_Synth2.pdf)
+ [17_RDD](imbens/272/17_RDD.pdf)


## 292 (2nd year, topics)

+ [maximum likelihood](imbens/292/slides_1_ml_1-1.pdf)
+ [discrete choice](imbens/292/slides_2_dc_1.pdf)
+ [discrete choice 2](imbens/292/slides_3_dc_2-1.pdf)
+ [bayes_1](imbens/292/slides_5_bayes_1_updated.pdf)
+ [bayes_2](imbens/292/slides_6_bayes_2_updated.pdf)
+ [efficiency_bounds](imbens/292/slides_8_efficiency_bounds.pdf)
+ [unconfoundedness](imbens/292/slides_9_unconfoundedness.pdf)
+ [trees and forests](imbens/292/slides_10-1.pdf)
+ [deep learning](imbens/292/slides_11-1.pdf)
+ [instrumental_variables_young](imbens/292/slides_11_instrumental_variables_young-3.pdf)
+ [panel data](imbens/292/slides_12-2.pdf)
+ [double ML](imbens/292/slides_13.pdf)
+ [weak instruments and many instruments](imbens/292/slides_14-3.pdf)
+ [peer effects](imbens/292/slides_14-5.pdf)
+ [con out of econometrics](imbens/292/slides_19-1.pdf)

## 293 (Athey / Wager)


+ [ML Pitch](atheyWager/1appliedml_1introduction.pdf)
+ [ML Basics](atheyWager/1appliedml_2secretsauce.pdf)
+ [Pred v Estimation](atheyWager/1appliedml_3predictionvsestimation.pdf)
+ [CI Basics: RA/IPW/AIPW](atheyWager/2stefan_week2_slides.pdf)
+ [HetFX : Causaltree](atheyWager/3a1_CATE_RCT_trees_novideo.pdf)
+ [Causaltree2](atheyWager/3a2_CATE_RCT_trees_novideo.pdf)
+ [GRF](atheyWager/3b_CATE_RCT_Forest_novideo_v2.pdf)
+ [HetFX : T/S/X/R learner](atheyWager/4stefan_week_4_slides.pdf)
+ [Robinson / Rlearner](atheyWager/5stefan_week_5_slides.pdf)
+ [Matrix Completion](atheyWager/6_1__MatrixCompletionNoVideo.pdf)
+ [MatrixFactorisation Structural](atheyWager/6_2__FactorizationChoicePanel_NoVideo.pdf)
+ [CVX: OptRD + SynthDID ](atheyWager/7lecture7_cvx.pdf)
+ [Nnets](atheyWager/8Session8-1IntroToNNnovideo.pdf)
+ [GANS intro](atheyWager/8Session8-2IntroToGANS.pdf)
+ [GANS simstudy](atheyWager/8Session8-3_WGANs_for_Simulation.pdf)
+ [DeepIV ](atheyWager/9.c.DeepIV.pdf)
