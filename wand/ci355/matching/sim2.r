## 
## Jonathan Wand : 2006-02-26
## simulation with ATT != ATE due to imbalance
##

library(Matching)

## not actually true treatment effect... 
## heterogeneous effect (see below)
delta <- 2
## set up 
N  <- 100
set.seed(391)

## characteristics of sample
x0<- c(0,1)
x <- c( rep(0,N), rep(1,N) )

## adjustment difference...
mu <- (1-x)*.5 + x*3
mu0<- c(.5,3)

## true propensity model
b0 <-  -3
b1 <-  5
logit <- function(x) 1/(1+exp(-(b0 + b1*x)))
## propensity score
f <- logit( x )
(ff<- logit( x0 ))

## treatment
w <- rbinom( 2*N, 1, p=f)
wf<- as.logical(w)
table( x,w)

## outcome
y <- mu + mu^2 * w*delta

## note this is the TRUE treatment effect by group:
 3 + 3^2 * 2  - 3
.5 + .5^2 * 2 - .5

## simple mean difference of two groups
mean(y[wf]) - mean(y[!wf])

## try matching...
rr  <- Match(Y=y,Tr=w,X=x,M=1,exact=TRUE,
             estimand = "ATT"    );
summary(rr)
table(rr$index.control)
x[rr$index.control]
x[rr$index.treated]

## very different from ATE!
rr  <- Match(Y=y,Tr=w,X=x,M=1,exact=TRUE,
             estimand = "ATE"    );
summary(rr)

## again, could use actual linear model
summary(z <- lm( y ~ w + x ))








