## 
## Jonathan Wand : 2006-02-26
## simulation of a linear effect in a quadratic model
## example of poor matching due to imbalance
##   -> correction with caliper
## 

## true treatment effect...
delta <- 1
## set up 
N  <- 100
set.seed(391)

## characteristics of sample
xf <- as.logical(c( rep(0,N), rep(1,N) ))
x1 <- c( rnorm(N), rnorm(N,2) )


## adjustment difference...
mu <- x1^2 

## true propensity model
b0 <-  0
b1 <-  1
logit <- function(x) 1/(1+exp(-(b0 + b1*x1)))
## propensity score
f <- logit( x1 )

## treatment
w <- rbinom( 2*N, 1, p=f)
wf<- as.logical(w)

plot(density( f[xf],from=0,to=1),xlim=c(0,1))
lines(density( f[!xf],from=0,to=1),col="red")

plot(density( x1[xf]),xlim=c(-2,5),ylim=c(0,1))
lines(density( x1[!xf]),col="red")

## outcome
y <- mu + w*delta
## simple mean difference of two groups
mean(y[wf]) - mean(y[!wf])

## mean in treatment and control
(ft <- mean( x1[ wf] ))
(fc <- mean( x1[!wf] ))

## approximate
(ft - fc)*(.5-2)


library(Matching)


## this isn't great, big standard error
rr  <- Match(Y=y,Tr=w,X=f,M=1,
             estimand = "ATT"    );
summary(rr)

## this is quite bad
rr  <- Match(Y=y,Tr=w,X=f,M=1,
             estimand = "ATE"    );
summary(rr)

## what is going on?
## think about what we are asking of the data
sort(table(rr$index.control))
plot(x1[ rr$index.treated ], x1[ rr$index.control ])

## instead of stretching, consider a caliper...
sd(f)
rr  <- Match(Y=y,Tr=w,X=f,M=1,
             estimand = "ATT"  , caliper=.1  );
summary(rr)
rr  <- Match(Y=y,Tr=w,X=f,M=1,
             estimand = "ATE"  , caliper=.1  );
summary(rr)

## this gets the opposite sign!
summary(z <- lm( y ~ w + x1 ))
plot(z)
## but get it exactly right with exact model
summary(z <- lm( y ~ w + x1 + I(x1^2)))

