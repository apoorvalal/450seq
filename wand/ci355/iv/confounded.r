##
## Jonathan Wand : 2006-03-04
## Example of simple confounded treatment
##

## library that facilitates random multivariate draws
library(mvtnorm)
## and for drawing ellipses
library(ellipse)
## and for IV estimation
library(systemfit)

# %%
## Define some constants
## how many simulations of the dgp/estimation
nsim <- 100
## how many cases
N <- 100
## the covariance between the residuals for selection mechanism and
## the outcome
sigma <- matrix( c(1,.8,.8,1),ncol=2)

## a possible instrument to be used later
## and its dichotomous transformation
Z <- rnorm(N)
Zd<- as.numeric( Z > 0)

# %%
## Let's look at the model slowly here:
## There are some correlated errors...
u <- rmvnorm(N, c(0,0), sigma=sigma)
## A treatment indicator (1=treatment)
W <- as.numeric( Z + u[,1] > 0 )
## An outcome, Treatment effect is 1 (tell me why)
Y <- W + u[,2]
# %%
## what would occur if we regressed Y on D?  (tell me why)
## Okay, now that you have said why, let's take a look.
## We will consider a bunch of scenarios in this simulation
## so let's make some places to save the treatment effect estimate:
tau.simple <- tau.reg <- tau.iv2 <- tau.iv <- rep(NA,nsim)
# %%
## this will repeatedly draw observations from the
## DGP (although Z is treated as fixed)
for (i in 1:nsim) {
  ## get data:
  u <- rmvnorm(N, c(0,0), sigma=sigma)
  W <- as.numeric( Z + u[,1] > 0 )
  Y <- W + u[,2]
  ## this estimates the sample difference in means
  lm.simple  <- lm( Y ~ W )
  ## this is an IV estimate
  ## where we first project W into the space of the instrument
  Wf   <- fitted(lm(W ~ Z))
  ## an use this projection as regressor
  lm.iv <- lm( Y ~ Wf )
  ## an alternative where we just use the sign info of Z as an instrument
  Wd<- fitted(lm(W ~ Zd))
  lm.iv2 <- lm( Y ~ Wd )
  ## finally let's just put the instrument into the regression...
  lm.reg <- lm( Y ~ W + Z )
  ## which regression would you believe a priori will
  ## give you the correct result? (why??)
  ## save the treatment effect estimates
  tau.simple[i] <- coef(lm.simple)[2]
  tau.iv[i]     <- coef(lm.iv )[2]
  tau.iv2[i]    <- coef(lm.iv2)[2]
  tau.reg[i]    <- coef(lm.reg)[2]
}
# %%
## this uses the correct resids...
sf.iv <- systemfit( Y ~ W , "2SLS",  inst= ~ Z)
summary(sf.iv)

# %%
res2 = cbind(tau.simple, tau.iv, tau.iv2, tau.reg)
plot(density(res2[,1]))
for(i in 2:ncol(res2)){
  lines(density(c(res2[,i], col = i)))
}

# %%
## let's take a look at the distribution of the treatment estimates now..
tt <- rbind(quantile(tau.simple),
      quantile(tau.iv),
      quantile(tau.iv2),
      quantile(tau.reg))
row.names(tt) <- c("simple","iv","iv2","reg")

## take a look
round(tt,3)

# %%
