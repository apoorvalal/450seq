##
## Jonathan Wand : 2006-02-26
## 
## Simulation of a mean shift treatment effect
## in an additive linear model
## 
## Note: no stochastic element across individuals for simplicity
## 

## true treatment effect...
delta <- 1
## set up 
N  <- 100
set.seed(391)

## characteristics of sample
x0<- c(0,1)
x <- c( rep(0,N), rep(1,N) )

## adjustment difference...
mu <- (1-x)*.5 + x*2
mu0<- c(.5,2)

## true propensity model
b0 <-  0
b1 <-  1
logit <- function(x) 1/(1+exp(-(b0 + b1*x)))
## propensity score
f <- logit( x )
ff<- logit( x0 )

## treatment
w <- rbinom( 2*N, 1, p=f)
wf<- as.logical(w)
table( x,w)

## outcome
y <- mu + w*delta
## simple mean difference of two groups
mean(y[wf]) - mean(y[!wf])

## distribution of 1s in treatment and control
ft <- mean( x[ wf] )
fc <- mean( x[!wf] )

## here is the bias, see Cochran (and derive it yourself too)
(ft - fc)*(.5-2)

## consider matching...
## ATT
library(Matching)
rr  <- Match(Y=y,Tr=w,X=x,M=1,exact=TRUE,
             estimand = "ATT"    );
summary(rr)

## ATE (should be same)
rr  <- Match(Y=y,Tr=w,X=x,M=1,exact=TRUE,
             estimand = "ATE"    );
summary(rr)

## could also use actual linear model
summary(z <- lm( y ~ w + x ))
