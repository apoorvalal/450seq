##
## Jonathan Wand : 2006-03-04
## Oh, so you want to use lags as instruments...
##

## library that facilitates random multivariate draws
library(mvtnorm)
## and for IV estimation
library(systemfit)
set.seed(391)

## Define some constants
## how many simulations of the dgp/estimation
nsim <- 200
## how many cases
N <- 200
## the covariance between the residuals for selection mechanism and
## the outcome
sigma <- matrix( c(1,.8,.8,1),ncol=2)
## serial correlation of innovations
rho<- 0.7

## we will use this below in a loop, but let's look at it first...
u <- rmvnorm(N, c(0,0), sigma=sigma)
## calc the lagged residual, given u[t] = rho * u[t-1] + e[t]
ulag <- (u[,1] - rnorm(N))/rho
## which looks like...
plot( u[,1] , ulag)

##
Z <- rnorm(N)
Zlag <- Z + ulag
Wlag <- as.numeric( Z + ulag  > 0 )
W    <- as.numeric( Z + u[,1] > 0 )
Y <- W + u[,2]


## so let's make some places to save the treatment effect estimate:
tau.simple <- tau.reg <- tau.iv2 <- tau.iv <- tau.iv3 <- rep(NA,nsim)

## this will repeatedly draw observations from the
## DGP (although Z is treated as fixed)
for (i in 1:nsim) {
  ## get data:
  u <- rmvnorm(N, c(0,0), sigma=sigma)
  Z <- rnorm(N)
  Zlag <- Z + ulag
  Wlag <- as.numeric( Z + ulag  > 0 )
  W    <- as.numeric( Z + u[,1] > 0 )
  Y <- W + u[,2]

  ## this estimates the sample difference in means
  lm.simple  <- lm( Y ~ W )

  ## this is an IV estimate
  ## where we first project W into the space of the instrument
  Wf   <- fitted(lm(W ~ Z))
  ## an use this projection as regressor
  lm.iv <- lm( Y ~ Wf )

  ## this is an IV estimate
  ## what about a lagged value... which is still correlated with
  ## stochastic component of outcome....
  Wd   <- fitted(lm(W ~ Wlag))
  ## an use this projection as regressor
  lm.iv2 <- lm( Y ~ Wd )

  
  ## this is an IV estimate
  ## what about a lagged value... which is still correlated with
  ## stochastic component of outcome....
  Wd   <- fitted(lm(W ~ Zlag))
  ## an use this projection as regressor
  lm.iv3 <- lm( Y ~ Wd )
  
  ## finally let's just put the instrument into the regression...
  lm.reg <- lm( Y ~ W + Wlag )

  ## which regression would you believe a priori will
  ## give you the correct result? (why??)

  ## save the treatment effect estimates
  tau.simple[i] <- coef(lm.simple)[2]
  tau.iv[i]     <- coef(lm.iv )[2]
  tau.iv2[i]    <- coef(lm.iv2)[2]
  tau.iv3[i]    <- coef(lm.iv3)[2]
  tau.reg[i]    <- coef(lm.reg)[2]
}

## let's take a look at the distribution of the treatment estimates now..
pp <- c(0,0.025,.25,.5,.75,.975,1.0)
tt <- rbind(quantile(tau.simple,p=pp),
      quantile(tau.iv,p=pp),
      quantile(tau.iv2,p=pp),
      quantile(tau.iv3,p=pp),
      quantile(tau.reg,p=pp))
row.names(tt) <- c("simple","iv","iv dich","iv cont","reg")

## take a look 
round(tt,3)
