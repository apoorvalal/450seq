## POLISCI 355 demo
## Annotated version of analysis of Lalonde data (CPS1)
## Original code by Sekhon & Diamond 
## Annotated with some additional analysis and diagnostics of underlying components of data.
## by Wand
##
## Important Note:
##   1. You will NEVER run an analysis with this small a population size
##      THIS IS A TOY version!!!
##   2. It get surprisingly close nonetheless
##   3. You will run an analysis with a population size 5000+ for non-demos!
##
## Source:
##    based on oct.genmatch.pscore 
##    orignally from lapo:/home/adiamond/midwest/DW/forJas/oct.genmatch.pscore
##

## make sure you are starting off fresh...
rm(list=ls())

## this is the core library....
## it will also load, and therefore REQUIRE! MASS library (standard)
## PLUS rgenoud
## these are available at CRAN using:
##   install.packages("rgenoud")
##   install.packages("Matching")
library(Matching)

## make this point to whereever you are keeping the data...
load("DW.cps1.re74.RData")

## should have 16177 cases
dim(DW.cps1.re74)

## note, rather sparse amount of information available on each person
## most importantly though, this file contains the Reported Earnings from 1974+1975 (pre-treatment)
## and 1978 (post-treatment)
names(DW.cps1.re74)

## renaming to make the object easier to type...
foo <- DW.cps1.re74
#to save space
rm(DW.cps1.re74)

## let's take a look at the data....
table(foo$treat)

(tb <- xtabs( ~ black + treat, data=foo))
round(tb / apply(tb,1,sum),3)

par(mfrow=c(2,1))
plot(density(foo$education[!as.logical(foo$treat)]),col="red")
lines(density(foo$education[as.logical(foo$treat)]),col="green")

plot(density(foo$age[as.logical(foo$treat)]),col="green")
lines(density(foo$age[!as.logical(foo$treat)]),col="red")

## this is the matrix that we are going to use for matching
## note rescaling of money:
X <-   as.matrix(cbind( foo$age,
                        foo$educ,
                        foo$black,
                        foo$hispan,
                        foo$married,
                        foo$nodegree,
                        I(foo$re74/1000),
                        #I(as.real(foo$re74 == 0)),
                        I(foo$re75/1000)))# ,
                        #I(as.real(foo$re75 == 0))))

## these are all the data, and interactions/transformations of the data that
## we would like to use for evaluating the quality of the balance between matched sets
## NOTE: criteria is much broader than just those that go into the Mahanolobis distance function
BalanceMat <- as.matrix(cbind(
                     foo$age,
                     foo$educ,
                     foo$black,
                     foo$hispan,
                     foo$married,
                     foo$nodegree,
                     I(foo$re74/1000),
                     I(foo$re75/1000),
                     I((foo$re74/1000)^2),
                     I((foo$re75/1000)^2),
                         
                     I((foo$age)^2),
                     I((foo$educ)^2),
                     I((foo$black)^2),
                     I((foo$hispan)^2),
                     I((foo$married)^2),
                     I((foo$nodegree)^2),
                         
                     I(foo$age*foo$educ),
                     I(foo$age*foo$black),
                     I(foo$age*foo$hispan),
                     I(foo$age*foo$married),    
                     I(foo$age*foo$nodegree),
                     I(foo$age*(foo$re74/1000)),
                     I(foo$age*(foo$re75/1000)),

                     I(foo$educ*foo$black),
                     I(foo$educ*foo$hispan),
                     I(foo$educ*foo$married),    
                     I(foo$educ*foo$nodegree),
                     I(foo$educ*(foo$re74/1000)),
                     I(foo$educ*(foo$re75/1000)),

                     I(foo$black*foo$hispan),
                     I(foo$black*foo$married),
                     I(foo$black*foo$nodegree),
                     I(foo$black*(foo$re74/1000)),
                     I(foo$black*(foo$re75/1000)),

                     I(foo$hispan*foo$married),
                     I(foo$hispan*foo$nodegree),
                     I(foo$hispan*(foo$re74/1000)),
                     I(foo$hispan*(foo$re75/1000)),

                     I(foo$married*foo$nodegree),
                     I(foo$married*(foo$re74/1000)),
                     I(foo$married*(foo$re75/1000)),
                                                
                     I(foo$nodegree*(foo$re74/1000)),
                     I(foo$nodegree*(foo$re75/1000)),                        

                     I((foo$re74/1000)*(foo$re75/1000))                                                 
                         ) )

### PROPENSITY SCORE

## This is the DW proposed p-score model
## from Review of Econ and Statistics 2002
model.A = foo$treat~I(foo$age) + I(foo$age^2) + I(foo$age^3) + I(foo$educ) + I(foo$educ^2) + I(foo$married) + I(foo$nodegree) + I(foo$black) + I(foo$hispan) + I(foo$re74) + I(foo$re75) + I(foo$re74 == 0) + I(foo$re75 == 0) + I(I(foo$re74)*I(foo$educ))

## put this into a logit...
pscores.A <- glm(model.A, family = binomial(link="logit"))

## Orthogonalize other values for weighted-MD wrt mu
## X2 will contain the residuals of the regression:
X2 <- X
for (i in 1:ncol(X2))
  {
   lm1 = lm(X2[,i]~pscores.A$linear.pred)
   X2[,i] = lm1$residual
   }

# VARIABLES WE MATCH ON BEGIN WITH PROPENSITY SCORE
# when binding them together for MD:
orthoX2.plus.pscore = cbind(pscores.A$linear.pred, X2)

## mean deviate p-score linear predictor
## now ALL columns in orthoX2.plus.pscore have mean zero...
orthoX2.plus.pscore[,1] <- orthoX2.plus.pscore[,1] -
                           mean(orthoX2.plus.pscore[,1])

qqnorm(pscores.A$fit)
qqline(pscores.A$fit)
qqnorm(pscores.A$linear.pred)
qqline(pscores.A$linear.pred)
qqnorm(orthoX2.plus.pscore[,1])
qqline(orthoX2.plus.pscore[,1])


# NORMALIZE ALL COVARS BY STANDARD DEVIATION
for (i in 1:(dim(orthoX2.plus.pscore)[2]))
  {
    orthoX2.plus.pscore[,i] <-
      orthoX2.plus.pscore[,i]/sqrt(var(orthoX2.plus.pscore[,i]))
  }

qqnorm(orthoX2.plus.pscore[,1])
qqline(orthoX2.plus.pscore[,1])


# SET BOUNDS ON GENMATCH DOMAIN (OPTIONAL)
## this is same as in genoud
lower <- c(95, rep(0, dim(orthoX2.plus.pscore)[2] - 1))
upper <- c(105, rep(1000, dim(orthoX2.plus.pscore)[2] - 1))
Domains <- as.matrix(cbind(lower, upper))
print(Domains)

set.seed(12345)

GM.out <-
  GenMatch(Tr = foo$treat,
           X = orthoX2.plus.pscore,
           BalanceMatrix = BalanceMat,
#           pop.size = 500,
           pop.size = 10,           
           max.generations = 25,
           wait.generations = 5,
           print.level = 3)

#benchmark=$1794
mout <- Match(Y=foo$re78,
              Tr = foo$treat,
              X = orthoX2.plus.pscore,
              Weight.matrix=GM.out,
              estimand = "ATT",
              M = 1
              )

## test for balance, get pre AND post matching when use match.out=''
# 'nboots' and 'nmc' are set to small values in the interest of speed.
# INCREASE TO AT LEAST 500 each for publication quality p-values.
mb  <- MatchBalance(treat~age + I(age^2) + education + I(education^2)
                    + black + hispan + married + nodegree + re74  + I(re74^2) + re75
                    + I(re75^2) ,
                    data=foo, match.out=mout, nboots=100)
             
summary(mout)

## what is in here...
names(mout)

## see that there are ties....
cbind(mout$weights,mout$index.treated,mout$index.control)

## can also pick out stuff
## if you had names, this would useful
## here, no names, but lets look at education
par(mfrow=c(2,1))
plot(density(foo$education[!as.logical(foo$treat)]),col="red")
lines(density(foo$education[as.logical(foo$treat)]),col="green")

plot(density(foo$education[mout$index.treated]),col="green")
lines(density(foo$education[mout$index.control]),col="red")

